<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
	function Add() {
		var username = $('#username').val();
		var password = $('#password').val();
		var data0 = {
			username : username,
			password : password
		};
		$.ajax({
			url : "adduser",
			type : "POST",
			data : JSON.stringify(data0),
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function(data) {
				var obj = JSON.parse(data);
			},
			error : function(error) {
				alert("Error !")
			}
		});
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello</title>
</head>
<body>
	<label>Username:</label>
	<input type="text" name="username" id="username">
	<br>
	<label>Password:</label>
	<input type="text" name="password" id="password">
	<br>
	<input type="button" onclick="Add()" value="Add"><br>
	<a href="nextpage">Next Page</a>
	
</body>
</html>