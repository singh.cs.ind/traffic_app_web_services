package com.nxtlife.dao;

import java.util.List;

import com.nxtlife.model.TrafficKeyword;

public interface KeywordDao {
	public void addKeyword(TrafficKeyword trafficKeyword);
	public List<TrafficKeyword> fetchKeyword(String keyword);
}
