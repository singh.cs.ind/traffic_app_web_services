package com.nxtlife.service;

import com.nxtlife.model.TrafficKeyword;

public interface AddKeywordService {
	public void addKeyword(TrafficKeyword trafficKeyword);

}
