package com.nxtlife.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nxtlife.dao.KeywordDao;
import com.nxtlife.model.TrafficKeyword;

@Service
@Transactional
public class AddKeywordServiceImpl implements AddKeywordService{
	@Autowired
	private KeywordDao dao;
	
	public void addKeyword(TrafficKeyword trafficKeyword){
		dao.addKeyword(trafficKeyword);
	}
	
}
