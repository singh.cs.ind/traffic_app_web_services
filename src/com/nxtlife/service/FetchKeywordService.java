package com.nxtlife.service;
import java.util.List;

import com.nxtlife.model.TrafficKeyword;

public interface FetchKeywordService {

	public List<TrafficKeyword> fetchKeyword(String keyword);
}
