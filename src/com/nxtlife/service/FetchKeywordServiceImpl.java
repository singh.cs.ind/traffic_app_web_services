package com.nxtlife.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nxtlife.model.TrafficKeyword;
import com.nxtlife.dao.KeywordDao;
import com.nxtlife.model.TrafficKeyword;

@Service
@Transactional
public class FetchKeywordServiceImpl implements FetchKeywordService{
	@Autowired
	private KeywordDao dao;
	
	public List<TrafficKeyword> fetchKeyword(String keyword){
		return dao.fetchKeyword(keyword);
	}
}
