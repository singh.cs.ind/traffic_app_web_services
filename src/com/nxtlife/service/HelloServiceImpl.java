package com.nxtlife.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nxtlife.dao.HelloDao;
import com.nxtlife.model.User;

@Service
@Transactional
public class HelloServiceImpl implements HelloService {
	@Autowired
	private HelloDao dao;
	public void addUser(User user){
		dao.addUser(user);
	}
}
